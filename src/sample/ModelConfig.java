package sample;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * Created by remy on 19/11/17.
 */
public class ModelConfig extends VBox {

    private Slider zoom = new Slider(1.0, 10.0, 1.0);
    public  Slider getZoom() {
        return zoom;
    }
    private Label  zoomLabel = new Label("Zoom : ");

   public ModelConfig(){

        zoom.setShowTickLabels(true);
        zoom.setShowTickMarks(true);
        zoom.setMajorTickUnit(10);
        zoom.setMinorTickCount(1);
        zoom.setBlockIncrement(1);

        this.setSpacing(10);
        this.setPadding(new Insets(30, 50, 50, 50));
        this.getChildren().add(zoomLabel);
        this.getChildren().add(zoom);


    }
}
