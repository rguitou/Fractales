package sample;

import javafx.scene.canvas.Canvas;

/**
 * Created by remy on 13/11/17.
 */
public class Model {
    private int LARGEUR = 4096;
    private int HAUTEUR = 2048;

    private Canvas fractale = new Canvas(LARGEUR, HAUTEUR);
    public Canvas getFractale() {
        return fractale;
    }

    public int getHAUTEUR() {
        return HAUTEUR;
    }
    public int getLARGEUR() {
        return LARGEUR;
    }
}
