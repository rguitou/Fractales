package sample;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.stage.Stage;
import javafx.scene.paint.*;
import javafx.scene.layout.StackPane;

public class Main extends Application {

    public ControllerFractale con = new ControllerFractale();


    private static final double RADIUS  = 200;
    private static final double VIEWPORT_SIZE = 1000;
    private static final double ROTATE_SECS   = 30;


    @Override
    public void start(Stage stage) {

        ModelSphere frac = new ModelSphere(RADIUS);
        ModelPan panneau = new ModelPan(frac);
        ControllerSphere cs = new ControllerSphere(panneau, frac);
        cs.changeZoom();
        Scene scene = new Scene(new StackPane(panneau), VIEWPORT_SIZE, VIEWPORT_SIZE, true, SceneAntialiasing.BALANCED);

        scene.setFill(Color.rgb(10, 10, 40));

        scene.setCamera(new PerspectiveCamera());
        stage.setScene(scene);
        stage.show();
        stage.setFullScreen(true);

        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth) {
                panneau.changeTaillePanSphere(newSceneWidth.doubleValue() - 300.0, panneau.getSub().getHeight());
            }
        });
        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                panneau.changeTaillePanSphere(panneau.getSub().getWidth(), newSceneHeight.doubleValue());
            }
        });

        cs.TourneSphere(scene);


    }

    public static void main(String[] args) {
        launch(args);
    }
}
