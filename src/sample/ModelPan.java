package sample;

import javafx.geometry.Rectangle2D;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Sphere;
import javafx.stage.Screen;

/**
 * Created by remy on 17/11/17.
 */
public class ModelPan extends BorderPane {

    private static final double VIEWPORT_SIZE = 1000;

    private Pane gp;

    public ModelConfig getConfig() {
        return config;
    }

    private ModelConfig config = new ModelConfig();

    private ModelSphere bouboule;

    public SubScene getSub() {
        return sub;
    }

    private SubScene sub;

    public ModelPan(ModelSphere bouboule){

        this.bouboule = bouboule;
        config.setMinWidth(300.0);
        config.setStyle("-fx-background-color: #D9D6C3;");
        gp = new Pane(bouboule);
        sub = new SubScene(gp, 1000, 1000, true, SceneAntialiasing.BALANCED);
        sub.setCamera(bouboule.getCam());

        this.bouboule.setTranslateX(sub.getWidth()/2);
        this.bouboule.setTranslateY(VIEWPORT_SIZE / 2);

        this.setRight(sub);
        this.setLeft(config);


    }

    public ModelPan(){
        super();
    }

    public void changeTaillePanSphere(double nouvelleLargeur, double nouvelleHauteur){
        sub.setWidth(nouvelleLargeur);
        sub.setHeight(nouvelleHauteur);
        bouboule.setTranslateX(sub.getWidth()/2);
        bouboule.setTranslateY(VIEWPORT_SIZE / 2);
    }

}
