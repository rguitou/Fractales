package sample;

import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.*;
import javafx.scene.canvas.*;
import javafx.scene.transform.Transform;

import java.awt.image.BufferedImage;


public class ControllerFractale{

    Model mod = new Model();

    public WritableImage getBuffer() {
        return buffer;
    }

    private WritableImage buffer = new WritableImage(mod.getLARGEUR(),mod.getHAUTEUR());

    public ControllerFractale() {
        GraphicsContext gc = mod.getFractale().getGraphicsContext2D();
        gc.setFill(Color.BLACK);
        gc.fillRect(0,0,mod.getLARGEUR(),mod.getHAUTEUR());
        gc.setStroke(Color.RED);
        gc.setLineWidth(1);
        mandelbrot(mod.getLARGEUR(), mod.getHAUTEUR(), 100, buffer);
    }

    public void genererFractale(GraphicsContext gc, double x, double y, double x2, double y2, int iteration){
        if (iteration > 0) {
            double xn = (x + x2) / 2 + (y2 - y) / 2;
            double yn = (y + y2) / 2 - (x2 - x) / 2;
            genererFractale(gc, x2, y2, xn, yn, iteration - 1);
            genererFractale(gc, x, y, xn, yn, iteration - 1);
        } else {
            gc.strokeLine(x, y, x2, y2);
        }
    }

    private void mandelbrot(int width, int height, int i_max, WritableImage buffer) {
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                    Color tmp = calculCouleur((float)(x - mod.getLARGEUR()/2.0)/500, (float)(y-mod.getHAUTEUR()/2.0)/500, i_max);
                    buffer.getPixelWriter().setColor(x,y,tmp);
            }
        }
    }

    public Color calculCouleur(float x, float y, int i_max){
            float cx = x;
            float cy = y;

            int i = 0;
            boolean sorti = false;
            while(i < i_max && !sorti){
                float nx = x*x - y*y + cx;
                float ny = 2*x*y + cy;
                x = nx;
                y = ny;

                if(x*x + y*y > 4)
                    sorti = true;
                ++i;
            }
            if(i == i_max) {
                return Color.BLACK;

            }else {
                Color tmp = Color.hsb((float)i/i_max, 0.5f, 1);
                return tmp;
            }
    }

    public static WritableImage Conv(Canvas can, double pixel) {
        WritableImage writableImage = new WritableImage((int)Math.rint(pixel*can.getWidth()), (int)Math.rint(pixel*can.getHeight()));
        SnapshotParameters spa = new SnapshotParameters();
        spa.setTransform(Transform.scale(pixel, pixel));
        return can.snapshot(spa, writableImage);
    }


}
