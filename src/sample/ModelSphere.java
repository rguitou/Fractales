package sample;

import javafx.scene.PerspectiveCamera;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;

/**
 * Created by remy on 19/11/17.
 */
public class ModelSphere extends Sphere {

    private final double RADIUS = 200;

    private final ControllerFractale con = new ControllerFractale();

    public PerspectiveCamera getCam() {
        return cam;
    }

    private final PerspectiveCamera cam = new PerspectiveCamera();

    public ModelSphere(double radius){
        super(radius);


        PhongMaterial earthMaterial = new PhongMaterial();
        earthMaterial.setDiffuseMap(
                (con.getBuffer())
        );
        this.setMaterial(
                earthMaterial
        );
    }
}
