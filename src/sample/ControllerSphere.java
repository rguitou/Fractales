package sample;

import javafx.animation.RotateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.*;
import javafx.scene.transform.Rotate;

/**
 * Created by remy on 19/11/17.
 */
public class ControllerSphere {

    private final double RADIUS = 200.0;

    private ModelSphere bouboule;

    private ModelPan panneau;



    public ControllerSphere(ModelPan panneau, ModelSphere bouboule){
        this.panneau = panneau;
        this.bouboule = bouboule;

    }

    public void changeZoom(){
        panneau.getConfig().getZoom().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov,
                                Number old_val, Number new_val) {
                bouboule.setScaleX(new_val.doubleValue());
                bouboule.setScaleY(new_val.doubleValue());
                bouboule.setScaleZ(new_val.doubleValue());
                if(new_val.doubleValue() < old_val.doubleValue())
                    bouboule.getCam().setTranslateZ(bouboule.getCam().getTranslateZ() + 5*bouboule.getScaleZ());
                else if (new_val.doubleValue() > old_val.doubleValue())
                    bouboule.getCam().setTranslateZ(bouboule.getCam().getTranslateZ() - 5*bouboule.getScaleZ());
            }
        });
    }

    public void TourneSphere(Scene sc){
        sc.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.Q) {
                    bouboule.setRotationAxis(Rotate.Y_AXIS);
                    bouboule.setRotate(bouboule.getRotate() + 1.0);
                }
                if (event.getCode() == KeyCode.D) {
                    bouboule.setRotationAxis(Rotate.Y_AXIS);
                    bouboule.setRotate(bouboule.getRotate() - 1.0);
                }
                if (event.getCode() == KeyCode.Z) {
                    bouboule.setRotationAxis(Rotate.X_AXIS);
                    bouboule.setRotate(bouboule.getRotate() - 1.0);
                }
                if (event.getCode() == KeyCode.S) {
                    bouboule.setRotationAxis(Rotate.X_AXIS);
                    bouboule.setRotate(bouboule.getRotate() + 1.0);
                }
            }
        });
    };

    }

